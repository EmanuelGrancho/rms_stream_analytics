﻿// Sample UDF which returns sum of two values.
function main(arg1) {
    try{
        if(isNaN(arg1))
            return -1;
                
        if(arg1 != null)
            return arg1.toFixed(4);
        else
            return -1;
            
    } catch(err){
        return -2;
    }
}